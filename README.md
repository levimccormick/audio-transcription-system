# My Audio Transcription System

A quick system to take audio files of conversations and convert them into readable transcripts.

## How it works

A user uploads an audio file to an S3 bucket.
A lambda function receives an event from that bucket, and initiates an AWS Transcribe job with the audio file.
The job outputs the transcription to a second bucket, which triggers another lambda that formats the transcription nicely using git clone git@gitlab.com:levimccormick/audio-transcription-system.git

