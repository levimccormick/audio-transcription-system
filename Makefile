validate:
	aws cloudformation validate-template --template-body file://infra/transcription-stack.yml

build:
	docker run --rm -it \
		-v `pwd`/app:/code:ro \
		-v `pwd`/output:/output \
		package-lambda
		
	aws s3 cp output/ s3://baseinfra-sourcebucket-1oedthneccqbl/audio-transcription/lambda/ --recursive
	
create: build
	aws cloudformation create-stack \
		--stack-name ats-stack \
		--template-body file://infra/transcription-stack.yml \
		--parameters \
			ParameterKey=ProjectTag,ParameterValue=audio-transcription \
			ParameterKey=CodePath,ParameterValue=lambda \
			ParameterKey=CodeVersion,ParameterValue=$$(ls output/) \
		--timeout-in-minutes 30 \
		--capabilities CAPABILITY_NAMED_IAM \
		--on-failure DELETE \
		--tags Key=project,Value=ats Key=version,Value=$$(git rev-parse HEAD) || true
		
update: build
	aws cloudformation update-stack \
		--stack-name ats-stack \
		--template-body file://infra/transcription-stack.yml \
		--parameters \
			ParameterKey=ProjectTag,ParameterValue=audio-transcription \
			ParameterKey=CodePath,ParameterValue=lambda \
			ParameterKey=CodeVersion,ParameterValue=$$(ls output/) \
		--capabilities CAPABILITY_NAMED_IAM \
		--tags Key=project,Value=ats Key=version,Value=$$(git rev-parse HEAD) || true