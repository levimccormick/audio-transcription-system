import os
import boto3

client = boto3.client('transcribe')

def bucket_notification(event, context):
    '''Receive the upload notification from the bucket and start the transcription job.'''
    
    print(event)
    
    for record in event['Records']:
        bucket=record['s3']['bucket']['name']
        key=record['s3']['object']['key']
        s3_uri = f's3://{bucket}/{key}'
        job_name = key.split('/')[-1].split('.')[0]
        print(f'Notified: {bucket}/{key}')
        
        
        #check if job already exists before processing
        
        print(f'Processing: {job_name}')
        response = client.start_transcription_job(
            TranscriptionJobName=job_name,
            LanguageCode='en-US',
            Media={
                'MediaFileUri': s3_uri
            },
            OutputBucketName=os.environ['DestinationBucket'],
            Settings={
                'ShowSpeakerLabels': True,
                'MaxSpeakerLabels': 10,
                'ShowAlternatives': False
            },
            JobExecutionSettings={
                'AllowDeferredExecution': True|False,
                'DataAccessRoleArn': os.environ['TranscribeRole']
            }
        )

    
    return {
        'statusCode': 200,
        'body': 'Hello from Lambda!'
    }